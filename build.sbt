name := "shapes-oo-scala"

version := "0.0.1"

scalaVersion := "2.11.7"

scalacOptions ++= Seq("-deprecation", "-feature", "-unchecked")

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.2.6" % Test,
  "org.scalacheck" %% "scalacheck" % "1.12.5" % Test,
  "org.scalaz" %% "scalaz-scalacheck-binding" % "7.2.0" % Test,
  "edu.luc.etl" %% "scalamu" % "0.4.2"

)
