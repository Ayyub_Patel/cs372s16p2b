package edu.luc.cs.laufer.cs372.shapes

import Structures._
import org.scalatest.FunSuite
class lawTests extends FunSuite{
  test("equality works") {
    assert((Rectangle(3,3): ShapeF[_] )=== (Rectangle(3,3): ShapeF[_]))
    assert((Ellipse(3,3): ShapeF[_] )=== (Ellipse(3,3): ShapeF[_]))
    assert((Location(0,0,Rectangle(3,3)): ShapeF[_] )=== (Location(0,0, Rectangle(3,3)): ShapeF[_]))
    assert((Group(Rectangle(3,3),Ellipse(1,2),Location(0,0,Rectangle(3,3))): ShapeF[_] )=== (Group(Rectangle(3,3),Ellipse(1,2),Location(0,0,Rectangle(3,3)): ShapeF[_])))



    //    constant(3) assert_=== constant(3)
  //  uminus(constant(3)) assert_=== uminus(constant(3))
  }

}

//Note: speccifying ShapeF[_] was necessary. Can that be changed so ShapeF accepts a non-anonymous type?