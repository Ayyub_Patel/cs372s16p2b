/**
  * Created by Brenner on 3/21/2016.
  */
package edu.luc.cs.laufer.cs372.shapes // for assert_=== to work on Option
import Behaviors._
import org.scalatest.FunSuite

class behaviorTest extends FunSuite {
  test("size works") {
    assert (fixtures.simpleEllipse cata size === 9)
    fixtures.complex2 cata size assert_=== 10
  }

}
