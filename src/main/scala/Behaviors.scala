package edu.luc.cs.laufer.cs372.shapes

import edu.luc.cs.laufer.cs372.shapes.Structures._

import scalamu.Algebra

object Behaviors {
 // @tparam A
 /* val BoundingBox: Algebra[ShapeF, Location] = {
    case Rectangle(a,b) => ShapeF[(Location(0,0,Rectangle(a,b)))]
  }*/


  val size: Algebra[ShapeF, Int] = {
    case Rectangle(_,_) => 1
    case Ellipse(_,_) => 1
    case Location(_,_,s) => 1 + s
    case Group(g) => 1 + g
  }



}
