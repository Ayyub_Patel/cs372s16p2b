package edu.luc.cs.laufer.cs372.shapes

import scalaz.{ Equal, Functor }
import scalamu._ // algebra types and injected cata method
object shapes {


  sealed trait ShapeF[+A]

  case class Ellipse[A](x: Int, y: Int) extends ShapeF[Nothing]

  case class Rectangle[A](x: Int, y: Int) extends ShapeF[Nothing]

  case class Location[A](x: Int, y: Int, shape: A) extends ShapeF[A]

  case class Group[A](shapes: A*) extends ShapeF[A]


 // implicit value (scalaz)

  implicit object ShapeFFunctor extends Functor[ShapeF] {

    def map[A, B](fa: ShapeF[A])(f: A => B): ShapeF[B] = fa match {
      case Ellipse(x, y) => Ellipse(x, y)
      case Rectangle(x, y) => Rectangle(x, y)
      case Location(x, y, shape) => Location(x, y, f(shape))
      case Group(shapes@_*) => Group(shapes.map(f): _*)
    }

  }
// Equality

  implicit val ShapeEqual: Equal[Shape] = Equal.equalA
   /* implicit def ShapeFEqual[A](implicit A: Equal[A]): Equal[ShapeF[A]] = Equal.equal {

      case (Rectangle(x, y), Rectangle(a,b) ) => A.equal(x,a)  && A.equal(y,b)

          case (Ellipse(x, y), Ellipse(a,b) ) => A.equal(x,a)  && A.equal(y,b)

          case (Location(x,y,z), Location(a,b,c) ) => A.equal(x, a) && A.equal(y,b) &&  A.equal(z,c)

        case (Group(s), Group(t)) => A.equal(s,t)
case _ => false
  }
*/
  //Fixpoint

  type Shape = µ[ShapeF]

  // Lower-case factory

  object ShapeFactory {

    def ellipse(x: Int, y: Int) = In[ShapeF](Ellipse(x, y))

    def rectangle(x: Int, y: Int) = In[ShapeF](Rectangle(x, y))

    def location(x: Int, y: Int, shape: Shape) = In[ShapeF](Location(x, y, shape))

    def group(shapes: Shape*): Shape = In[ShapeF](Group(shapes: _*))

  }

}